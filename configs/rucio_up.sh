#!/bin/bash
DATASET=user.mguth.dl1.hp.optimisation.configs40
rucio add-dataset ${DATASET}


for i in `seq 0 39`;
do
  CONFIG=HP_configs_40_${i}.json
  rucio upload ${CONFIG} --name DL1_config_hp40_${CONFIG} --rse UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
  rucio attach ${DATASET} user.mguth:DL1_config_hp40_${CONFIG}

done
