# python train.py --use_gpu --configs configs/config_new_1.json,configs/config_new_2.json,configs/config_new_3.json --validationfile /work/ws/atlas/mg294-b_tagging/new_tuples/hybrids/test_hybrid_merged-more_stat_100-reduced.h5 --validation_config ttbar_more-stat.json --variables DL1_Variables.json --outputfile test.json --trainingfile /work/ws/atlas/mg294-b_tagging/new_tuples/hybrids/preprocessed/training_ttbar_more-stat_rebinned-reduced.h5
# TRAINFILE=/work/ws/atlas/mg294-b_tagging/ntuples_2018/PFlow/new_taggers/MC16d/hybrids/preprocessed/DL1Training-PFlow_extended-hybrid_MC16D_2018_70-4M_preprocessed.h5
# VALIDFILE=/work/ws/atlas/mg294-b_tagging/ntuples_2018/PFlow/new_taggers/MC16d/hybrids/MC16d_hybrid_odd_100_PFlow-validation.h5
# TRAINFILE=/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/DL1Training-PFlow_extended-hybrid_MC16D_2018_70-4M_preprocessed.h5
# VALIDFILE=/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/MC16d_hybrid_odd_100_PFlow-validation.h5
# VARS=DL1_framework/Training/DL1r_Variables.json
# SCALES=DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-4M_mu.json
TRAINFILE=/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/DL1Training-PFlow_extended-hybrid_MC16D_2018_70-22M_preprocessed-stats_file-merged.h5
VALIDFILE=/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/MC16d_hybrid_odd_100_PFlow-validation.h5
VARS=DL1_framework/Training/configs/DL1r_Variables.json
SCALES=DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-8M_mu.json
python train.py  --use_gpu --epochs 2 --large_file --configs test_conf.json --validationfile ${VALIDFILE} --validation_config ${SCALES} --variables ${VARS} --outputfile test.json --trainingfile ${TRAINFILE}
