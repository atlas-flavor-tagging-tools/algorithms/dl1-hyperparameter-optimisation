import subprocess
import sys
import os
sys.path.append("DL1_framework/tools")
import bsub_tools as bt

epochs = 130



TRAINFILE = "/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/DL1Training-PFlow_extended-hybrid_MC16D_2018_70-22M_preprocessed-stats_file-merged.h5"
VALIDFILE = "/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/MC16d_hybrid_odd_100_PFlow-validation.h5"
VARS= "/gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation/DL1_framework/Training/configs/DL1r_Variables.json"
SCALES = "/gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation/DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-8M_mu.json"


singularity_image = "singularity exec --pwd /gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation --nv --contain -B /gpfs,/scratch docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/ml-gpu:latest"

# make a directory to store training output in
try:
    # Create target Directory
    os.mkdir("results")
    os.mkdir("run")
    os.mkdir("logs")
    print("Creating run and result directory")
except:
    pass

# for number in range(90):
# for number in range(1, 10):
for number in range(6):

    command = "python train.py  --use_gpu --epochs %i --large_file \
--configs /gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation/configs/DL1r-layers_only_%i.json \
--validationfile %s --validation_config %s --variables %s \
--outputfile /gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation/results/DL1r_Pflow-HP-resultslayers_only_%i.json \
--trainingfile %s" % (epochs, number, VALIDFILE, SCALES, VARS, number,
                      TRAINFILE)
#     command = "python train.py  --use_gpu --epochs %i --large_file \
# --configs /gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation/configs/DL1r_PFlow_%i.json \
# --validationfile %s --validation_config %s --variables %s \
# --outputfile /gpfs/slac/atlas/fs1/u/mg294/workspace/dl1-hyperparameter-optimisation/results/DL1r_Pflow-HP-results%i.json \
# --trainingfile %s" % (epochs, number, VALIDFILE, SCALES, VARS, number,
#                       TRAINFILE)

    # jobs = bt.bsub(command, sh_name='run/run_HP-opt%i.job' % number)
    jobs = bt.bsub(command, sh_name='run/run_HP-opt-layers_only-%i.job' % number)
    jobs.singularity_image = singularity_image
    # jobs.run_time = "48:00"
    jobs.run_time = "30:00"
    process = jobs.Write()
    print(process)

    # checking if the job already exists or failed
    if process == 0:
        exit(0)
    # Launching the jobs
    else:
        test = subprocess.Popen(process, shell=True)
        test.wait()
