#!/bin/bash
TEST=/work/ws/atlas/mg294-b_tagging/new_tuples/hybrids/test_hybrid_merged-more_stat_100-reduced.h5
TRAIN=/work/ws/atlas/mg294-b_tagging/new_tuples/hybrids/preprocessed/training_ttbar_more-stat_rebinned-reduced.h5
VALIDCONF=ttbar_more-stat.json
VARS=DL1_Variables.json

#rucio upload ${TEST} --name DL1_files_hp_test --rse UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
#rucio attach user.mguth.dl1.hp.optimisation.files user.mguth:DL1_files_hp_test

DATASET=user.mguth.dl1.hp.optimisation.files.DL1r.PFlow
#rucio add-dataset ${DATASET}

rucio upload ${TRAIN} --name DL1_files_hp_training --rse UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
rucio attach user.mguth.dl1.hp.optimisation.files user.mguth:DL1_files_hp_training

rucio upload ${VALIDCONF} --name DL1_files_hp_validconf --rse UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
rucio attach user.mguth.dl1.hp.optimisation.files user.mguth:DL1_files_hp_validconf

rucio upload ${VARS} --name DL1_files_hp_variables --rse UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
rucio attach user.mguth.dl1.hp.optimisation.files user.mguth:DL1_files_hp_variables

