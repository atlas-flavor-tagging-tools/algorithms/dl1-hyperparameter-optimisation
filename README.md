# Hyperparameter Optimisation for DL1

This HP optimisation is desgined to run on the grid.

The main script is ```train.py``` with the following options
```
usage: train.py [-h] [--configs CONFIGS] [--trainingfile TRAININGFILE]
                [--validationfile VALIDATIONFILE]
                [--validation_config VALIDATION_CONFIG]
                [--variables VARIABLES] [--outputfile OUTPUTFILE]
```
* configs are a bunch of json files, in which the different HP are defined. They have to be passed like ```--configs config_1.json,config_2.json,config_3.json``` or with the bash synthax e.g. ```folder/*.json```.
* trainingfile: path to h5 file for training
* validationfile: path to h5 file for testing
* validation_config: json file with scaling, shifting and default values for the input variables
* variables: json file with list of variables which are used for training
* outputfile: defines the json outputfile containing the training and validation performance of the models

## Generation of Config Files

In order to generate the config files you can use the script ```Config_Generator.py```
```
usage: Config_Generator.py [-h] [--dicts_per_file DICT_PER_FILE]
                           [--config CONFIG] [--output_base OUTPUT_BASE]
```
With the option ```--config``` one passes a json file with the following synthax
```
{
   "lr": [0.1, 0.01, 20],
   "units": [[256, 512], [256, 512], 24, 12, 6],
   "activations": ["relu", "linear"],
   "batch_size": [100, 3000, 30]
 }
```
 for the learning rate there are 2 supported synthaxes:
```
"lr": [0.1, 0.01, 20]
```
will use the function numpy.linspace(*[0.1, 0.01, 20]) to generate different learning rates and
```
"lr": [[0.1, 0.01]]
```
will take the inner list as different learning rates. The same applies for ```batch_size```.

The units define the number of hidden layers as well as the used units within the hidden layer. It is possible to pass a flat list which fully defines the amount and units of Dense layers.
Passing a list within the list then varies the units using the entries of the inner list, e.g.
```
"units": [[30, 36, 42],24, 12, 6]
```
means the architecture has 4 hidden Dense layers and for the first layer the units are either 30, 36 or 42 (they will be permutaded).

In the case of the activation functions, one can either give a single string, a flat list e.g.
```
"activations": ["relu", "linear"]
```
which will result in a simoultaneous usage of the activation function in each layer but varied for different HP options.
The second option is then a list within the list
```
"activations": [["relu", ["linear", "relu"], "relu","relu"]]
```
which will assign each layer the specified activation function. NOTE: the length of hidden layers and activation functions has to match in this case.
